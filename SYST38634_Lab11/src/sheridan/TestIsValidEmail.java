package sheridan;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import phones.SmartPhone;

class TestIsValidEmail {

	//1.	Email must be in this format: <account>@<domain>.<extension>
	@Test
	void testEmailFormatGood() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.com");
		assertTrue("Good", price.equals("qwe@email.com"));
	}
	
	@Test
	void testEmailFormatBad() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qweemail.com");
		assertTrue("Bad", price.equals("qweemail.com"));
	}
	
	@Test
	void testEmailFormatBoundary() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.com");
		assertTrue("Boundary", price.equals("qwe@email.com"));
	}
	
	//2.	Email should have one and only one @ symbol.
	@Test
	void testHaveAtGood() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.com");
		assertTrue("Good", price.equals("qwe@email.com"));
	}
	
	@Test
	void testHaveAtBad() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qweemail.com");
		assertTrue("Bad", price.equals("qweemail.com"));
	}
	
	@Test
	void testHaveAtBoundary() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.com");
		assertTrue("Boundary", price.equals("qwe@email.com"));
	}
	
	//3.	Account name should have at least 3 alpha-characters in lowercase (must not start with a number).
	@Test
	void testAccountNameGood() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwer@email.com");
		assertTrue("Good", price.equals("qwer@email.com"));
	}
	
	@Test
	void testAccountNameBad() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qw@email.com");
		assertTrue("Bad", price.equals("qw@email.com"));
	}
	
	@Test
	void testAccountNameBoundary() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.com");
		assertTrue("Boundary", price.equals("qwe@email.com"));
	}
	
	//4.	Domain name should have at least 3 alpha-characters in lowercase or numbers.
	@Test
	void testDomainNameGood() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.com");
		assertTrue("Good", price.equals("qwe@email.com"));
	}
	
	@Test
	void testDomainNameBad() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@e.com");
		assertTrue("Bad", price.equals("qwe@e.com"));
	}
	
	@Test
	void testDomainNameBoundary() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@eml.com");
		assertTrue("Boundary", price.equals("qwe@eml.com"));
	}
	
	//5.	Extension name should have at least 2 alpha-characters (no numbers).
	@Test
	void testExtensionNameGood() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.com");
		assertTrue("Good", price.equals("qwe@email.com"));
	}
	
	@Test
	void testExtensionNameBad() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.c");
		assertTrue("Bad", price.equals("qwe@email.c"));
	}
	
	@Test
	void testExtensionNameBoundary() {
		isValidEmail email = new isValidEmail();
		String price = email.emailFormat("qwe@email.ca");
		assertTrue("Boundary", price.equals("qwe@email.ca"));
	}
	
}

package sheridan;

public class isValidEmail {

	//1.	Email must be in this format: <account>@<domain>.<extension>
	public String emailFormat(String emailString) {
		char[] es = new char[emailString.length()];

		int at = 0;
		
		for(int i=0;i<emailString.length();i++) {
			es[i] = emailString.charAt(i);
		}
		
		for(int i=0;i<emailString.length();i++) {
			if(es[i]=='@') {
				at = i;
			}
		}
		
		if(at != 0 && at != emailString.length()) {
			System.out.println("right format");
		}
		
		return emailString;
	}
		
	//2.	Email should have one and only one @ symbol.
	public String haveAt(String emailString) {
		char[] es = new char[emailString.length()];

		int at = 0;
		
		for(int i=0;i<emailString.length();i++) {
			es[i] = emailString.charAt(i);
		}
		
		for(int i=0;i<emailString.length();i++) {
			if(es[i]=='@') {
				at++;
			}
		}
		
		if(at>1) {
			System.out.println("have one more @");
		}
		
		return emailString;
	}
		
	//3.	Account name should have at least 3 alpha-characters in lowercase (must not start with a number).
	public String accountName(String emailString) {
		char[] es = new char[emailString.length()];

		int accountChar = 0;
		
		for(int i=0;i<emailString.length();i++) {
			es[i] = emailString.charAt(i);
			if(Character.isUpperCase(es[i])) {
				accountChar++;
			}
		}
		
		if(accountChar >= 3) {
			System.out.println("have at least 3 alpha-characters in lowercase");
		}
		
		return emailString;
	}
		
	//4.	Domain name should have at least 3 alpha-characters in lowercase or numbers.
	public String domainName(String emailString) {
		char[] es = new char[emailString.length()];

		int domainChar = 0;
		
		for(int i=0;i<emailString.length();i++) {
			es[i] = emailString.charAt(i);
			if(Character.isUpperCase(es[i])) {
				domainChar++;
			}
		}
		
		if(domainChar >= 3) {
			System.out.println("have at least 3 alpha-characters in lowercase");
		}
		
		return emailString;
	}
		
	//5.	Extension name should have at least 2 alpha-characters (no numbers).
	public String extensionName(String emailString) {
		char[] es = new char[emailString.length()];

		int extensionChar = 0;
		
		for(int i=0;i<emailString.length();i++) {
			es[i] = emailString.charAt(i);
			if(Character.isUpperCase(es[i])) {
				domainChar++;
			}
		}
		
		if(extensionChar >= 2) {
			System.out.println("have at least 2 alpha-characters");
		}
		
		return emailString;
	}
		
}
